<?php

class FusionDirectory
{
    private const LOGIN_ENDPOINT = '/v1/login';
    private const SEARCH_USER_ENDPOINT = '/v1/objects/user';

    private $curlWrapperFactory;
    public function __construct(CurlWrapperFactory $curlWrapperFactory)
    {
        $this->curlWrapperFactory = $curlWrapperFactory;
    }

    public function search(string $url, string $password, $filter, $output_attribute): string
    {
        $curlWrapper = $this->curlWrapperFactory->getInstance();
        $payload = [
            'directory' => 'default',
            'user' => 'read-account',
            'password' => $password,
        ];
        $curlWrapper->setJsonPostData($payload);
        $token = $curlWrapper->get(sprintf("%s%s", $url, self::LOGIN_ENDPOINT));
        $token = json_decode($token, true);
        $curlWrapper = $this->curlWrapperFactory->getInstance();
        $curlWrapper->addHeader('Session-token', $token);
        $filter_url = sprintf(
            "%s%s?filter=%s&attrs=$output_attribute",
            $url,
            self::SEARCH_USER_ENDPOINT,
            urlencode($filter)
        );
        $info = $curlWrapper->get($filter_url);
        $data = json_decode($info, true);
        if (!is_array($data) || count($data) != 1) {
            throw new UnrecoverableException("Impossible de récupérer les données de l'utilisateur : " . $info);
        }
        return reset($data);
    }
}
