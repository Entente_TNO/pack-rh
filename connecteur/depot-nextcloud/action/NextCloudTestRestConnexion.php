<?php

class NextCloudTestRestConnexion extends ActionExecutor
{
    public function go()
    {
        /** @var Nextcloud $nextCloud */
        $nextCloud = $this->getMyConnecteur();
        $result = $nextCloud->searchUser();
        $this->setLastMessage(
            "Récupération des données de l'utilisateur en login : <pre>" . htmlentities($result) . "</pre>"
        );
        return true;
    }
}
