DOCKER_COMPOSE=docker-compose
DOCKER_COMPOSE_UP=$(DOCKER_COMPOSE)  up -d
DOCKER_COMPOSE_EXEC=$(DOCKER_COMPOSE) exec web

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

docker-compose-up: ## Up all container
	$(DOCKER_COMPOSE_UP)

